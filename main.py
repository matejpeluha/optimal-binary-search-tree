from data_module import get_data
from tree_module import get_tree
from compare_module import get_compare_count

data = get_data()
results = get_tree(data)

print('w: ', results.w[0][-1])
print('tree cost: ', results.cost[0][-1])

root_id = results.roots[0][-1]
print('root id: ', root_id)
print('root word: ', data.keys[root_id])

word = input('\nWrite word: ')
compare_count = get_compare_count(word, data.keys, results.roots)
print('compare count: ', compare_count)
