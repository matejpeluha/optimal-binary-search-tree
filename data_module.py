class Data:
    def __init__(self):
        self.keys = []
        self.key_frequencies = []
        self.dummies = []
        self.dummy_frequencies = []
        self.frequency_sum = 0
        self.p = []
        self.q = []


def get_data():
    data = Data()
    fill_data(data)
    calculate_probabilities(data)

    return data


def fill_data(data: Data):
    with open('dictionary.txt') as f:
        lines = f.readlines()
    for line in lines:
        frequency, word = line.split(' ')
        frequency = int(frequency)
        if frequency > 50_000:
            data.keys.append(word.replace('\n', ''))
            data.key_frequencies.append(frequency)
        else:
            data.dummies.append(word.replace('\n', ''))
            data.dummy_frequencies.append(frequency)
        data.frequency_sum += frequency

    keys, key_frequencies = zip(*sorted(zip(data.keys, data.key_frequencies)))
    dummies, dummy_frequencies = zip(*sorted(zip(data.dummies, data.dummy_frequencies)))
    data.keys = keys
    data.key_frequencies = key_frequencies
    data.dummies = dummies
    data.dummy_frequencies = dummy_frequencies


def calculate_probabilities(data):
    for frequency in data.key_frequencies:
        data.p.append(frequency / data.frequency_sum)

    key_index = 0
    q_sum = 0
    for dummy_index in range(len(data.dummies)):
        dummy = data.dummies[dummy_index]
        dummy_frequency = data.dummy_frequencies[dummy_index]
        if key_index == len(data.keys) or dummy < data.keys[key_index]:
            q_sum += dummy_frequency
        else:
            q_i = q_sum / data.frequency_sum
            data.q.append(q_i)
            q_sum = 0
            key_index += 1
            if key_index == len(data.keys) or dummy < data.keys[key_index]:
                q_sum += dummy_frequency
                continue
    q_i = q_sum / data.frequency_sum
    data.q.append(q_i)
