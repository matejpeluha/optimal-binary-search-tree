from data_module import Data


class Results:
    def __init__(self, data: Data):
        q_length = len(data.q)
        keys_length = len(data.keys)
        self.cost = [[0 for _ in range(q_length)] for _ in range(q_length)]
        self.w = [[0 for _ in range(q_length)] for _ in range(q_length)]
        self.roots = [[0 for _ in range(keys_length)] for _ in range(keys_length)]


def get_tree(data: Data):
    results = Results(data)

    write_chains_with_one_node(data, results)
    write_chains_with_more_nodes(data, results)

    return results


def write_chains_with_one_node(data: Data, results: Results):
    """Fill matrices diagonally"""
    for i in range(len(data.q)):
        results.cost[i][i] = data.q[i]
        results.w[i][i] = data.q[i]


def write_chains_with_more_nodes(data: Data, results: Results):
    """Fill upper half of matrices"""
    for chain_length in range(len(data.keys)):
        write_chains_with_length(data, results, chain_length)


def write_chains_with_length(data: Data, results: Results, chain_length: int):
    keys_length = len(data.keys)
    for row_index in range(keys_length - chain_length):
        column_index = row_index + chain_length + 1
        results.w[row_index][column_index] = results.w[row_index][column_index - 1] + data.p[column_index - 1] + data.q[column_index]
        find_best_root(results, row_index, column_index)


def find_best_root(results: Results, row_index: int, column_index: int):
    minimal_cost = 2_147_483_647
    best_root_index = -1
    for root_index in range(row_index, column_index):
        subtree_cost = results.cost[row_index][root_index] + results.cost[root_index + 1][column_index] + results.w[row_index][column_index]
        if subtree_cost < minimal_cost:
            minimal_cost = subtree_cost
            best_root_index = root_index

    results.cost[row_index][column_index] = minimal_cost
    results.roots[row_index][column_index - 1] = best_root_index


def sum_frequencies(freq, i, j):
    s = 0
    for k in range(i, j + 1):
        s += freq[k]
    return s
