def get_compare_count(word: str, keys: [], roots: []):
    count = 0
    low_range_id = 0
    high_range_id = len(keys) - 1

    while low_range_id <= high_range_id:
        root_id = roots[low_range_id][high_range_id]
        root = keys[root_id]
        print(root)
        if word > root:
            low_range_id = root_id + 1
        elif word < root:
            high_range_id = root_id - 1
        else:
            return count + 1
        count += 1

    return count + 1
